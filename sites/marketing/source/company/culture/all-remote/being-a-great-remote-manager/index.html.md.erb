---
layout: handbook-page-toc
title: "Being a great remote manager"
canonical_path: "/company/culture/all-remote/being-a-great-remote-manager/"
description: "On this page, GitLab details considerations for being an excellent remote manager. Learn more!"
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab all-remote team](/images/all-remote/GitLab-All-Remote-Zoom-Team-Tanuki.jpg){: .shadow.medium.center}

On this page, we're detailing considerations for being an excellent remote manager. Many traits found in superb remote managers are also found in managers of colocated teams, though there are nuances to serving, leading, and guiding when managing teams that you do not see in-person each day.

## Traits of a great remote manager

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/S197ok0lBtw?start=431" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) video above, GitLab's Head of Remote discusses the topic of cultural maturity during an interview with [Mårten Mickos](https://twitter.com/martenmickos), CEO of [HackerOne](https://twitter.com/Hacker0x01).*

Managing remotely is much like managing in-person, but there are certain traits of outsized importance for the former.

### Self-awareness

Self-awareness is critical for relationship building and trust. The reality is that people prefer to learn, and to be managed, differently. GitLab's CEO goes so far as to [publicize his communication preferences](/handbook/ceo/#communication) and [flaws](/handbook/ceo/#flaws), which requires a high degree of self-awareness, a [low level of shame](/handbook/values/#low-level-of-shame), and a penchant for [transparency](/handbook/values/#transparency). 

Self-aware managers will be open with reports on their learning and communication preferences, enabling those who report to them to interact without ambiguity. 

Be highly sensitive to micromanaging. Particularly for new remote managers, you may be inclined to "check in" on projects with increased frequency given the inability to see someone working in the same physical space. This is a destructive practice. Instead, have an open discussion with a direct report on communication and work styles, and find a mechanism that suits all parties. 

What a manager perceives as proactively working to keep a project on track can be received as toxic micromanagement by a direct report. Without an open channel to communicate preferences, this can quickly [erode](/handbook/values/#five-dysfunctions) a working relationship. 

### Empathy

Empathy and [kindness](/handbook/values/#kindness) are core to being a great remote manager. It can be challenging to put yourself in the shoes of a direct report using [text communication](/company/culture/all-remote/effective-communication/) and [Zoom calls](/blog/2019/08/05/tips-for-mastering-video-calls/). In-person interactions allow for body language to be more easily read. In a remote setting, managers must instead be proactive in *asking* direct reports how life is going and what their learning preferences are. 

GitLab [gives people agency](/handbook/values/#give-agency) and trusts each team member to embrace [ownership](/handbook/values/#ownership), and act as a [manager of one](/handbook/values/#managers-of-one). 

For a greater understanding of the importance of empathy in a manager, read [GitLab's guide to combating burnout, isolation, and anxiety](/company/culture/all-remote/mental-health/). 

### Servant-leader qualities

Working to have [no ego](/handbook/values/#no-ego), recognizing that [people are not their work](/handbook/values/#people-are-not-their-work), and having [short toes](/handbook/values/#short-toes) will go a long way to building trust as a manager. The humility required to be a [servant-leader](https://www.shrm.org/resourcesandtools/hr-topics/organizational-and-employee-development/pages/the-art-of-servant-leadership.aspx) is rare, and is of great importance in a remote setting. Particularly for reports who are [acclimating to their first remote role](/company/culture/all-remote/getting-started/), managers may need to go above and beyond to lead by example. 

In many cases, reports will be discovering in real-time how they prefer to be managed remotely. Maintaining the perspective that managers excel by serving is critical to building confidence in a direct report. 

People tend to feel more guilty about asking a manager for step-by-step guidance in a remote setting — e.g. "I'm bothering them in their home!" To proactively address this, be sure to reinforce that you (as a manager) are not bothered by sincere requests for assistance. 

In sum, remote managers should operate from a standpoint of [wanting others to succeed](/handbook/values/#see-others-succeed). In the event that critical feedback must be delivered, strive to [surface issues constructively](/handbook/values/#surface-issues-constructively) and [do so in a 1-1 setting](/handbook/values/#negative-feedback-is-1-1). 

### A bias for documentation

Managers are often stretched for time. A critical, though common, mistake is to assume that you can earn back time by not communicating in full to one's direct reports. Great remote managers will devote time to [writing things down](/handbook/values/#write-things-down). GitLab's [handbook-first approach](/handbook/handbook-usage/#why-handbook-first) to documentation encourages managers to contextualize thoughts in text. 

Transmitting expectations, updates, and feedback through text is highly respectful. It enables a direct report to ingest information at their own pace, and it removes margin for misinterpretation. Written words are more easily [questioned](/handbook/values/#anyone-and-anything-can-be-questioned), thereby creating a more direct path to absolute truth and understanding. 

### Build Trust

To be a successful leader of remote teams, one must develop a [level of trust](/handbook/leadership/building-trust/) in each team. A trustworthy leader of remote teams consistently [provides feedback](/handbook/values/#give-feedback-effectively) to enable team members to feel included, valued, empowered, and respected. 

A remote leader must be intuitive and able to adapt to the preferences of their direct reports. Some team members prefer more or less communication from their leader, some need consistent affirmation, others prefer autonomy. The ability to ask about and adapt to these preferences is crucial. Many of these elements are viewed as unspoken needs in other organizations, but great leaders seek to clarify and remove ambiguity. This is a key element of servant leadership. 

Being a remote manager means building a support system for your team, while at the same time striking a balance to hold them accountable. Building trust and maintaining transparency, frequent and open communication, and ensuring a safe working environment are critical skills. 

Use weekly [1-1 meetings](/handbook/leadership/1-1/) to discuss business topics, challenges, and focus areas to build trust. Managers can supplement formal meetings with [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats) where no business is discussed. Listening and sharing during these discussions can facilitate more open conversations. Consider structing team meetings with a social component where team members can share the personal side of themselves. 

## Prioritize onboarding

![GitLab onboarding illustration](/images/all-remote/gitlab-commit-illustration.jpg){: .medium.center}

Onboarding is critical in equipping a new report with the tools and understanding they need to thrive at a company. A manager must be intentional about setting up guardrails to ensure that onboarding is not derailed. This is enabled through a long-term mindset. The depth and thoroughness of onboarding — as well as how *much* onboarding a new hire is cleared to complete — is [linked](https://hbr.org/2018/12/to-retain-new-hires-spend-more-time-onboarding-them) to long-term success. 

There is always work to be done, and a manager must make a conscious decision to allow a new hire to focus on onboarding instead of work during the critical early weeks, beliving that in doing so, they are enabling long-term efficiencies and prioritizing that over short-term task elimination. 

Numerous [studies](https://www.talentlms.com/blog/new-employee-onboarding-study/) have [shown](https://www.shrm.org/resourcesandtools/hr-topics/talent-acquisition/pages/onboarding-key-retaining-engaging-talent.aspx) that most employers rank poorly in onboarding quality, despite realities that losing an employee to poor onboarding is not cheap and a strong onboarding process boosts new hire retention and productivity. 

### Select the right Onboarding Buddy

GitLab's use of [Onboarding Buddies](/handbook/people-group/general-onboarding/onboarding-buddies/) is critical to the overall success of onboarding. 

The manager should be intentional about selecting an onboarding buddy. Aim to select an onboarding buddy that complements the new hire. For example, if the new hire is inexperienced in GitLab, consider selecting an onboarding buddy who is proficient in using and teaching GitLab. If the new hire has never worked remotely before, consider selecting an onboarding buddy with a history of working remotely. 

### Balance self-learning and nurturing 

In a remote setting, it's vital that a new hire recognize the importance of [working handbook-first](/company/culture/all-remote/handbook-first-documentation/). This reality needs to be balanced with nurturing — an empathetic approach to working with a colleague. During onboarding, ask for feedback on this. A manager should be willing and able to adapt to a new hire's preferred communication methods, and be willing to [iterate](/handbook/values/#iteration) on this. 

## Manage process

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SP7u0gYCHiY?start=608" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/SP7u0gYCHiY) above, Darren Murph, Head of Remote at GitLab sits down with Jeff Frick for a [Digital CUBE Conversation](https://siliconangle.com/2020/05/01/all-remote-gitlab-offers-advice-and-resources-as-companies-adjust-to-life-away-from-offices-cubeconversations/) about the way the global Covid-19 crisis is affecting the way people work, and work from home. Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

A natural inclination when managing a team is to manage people — the *individuals*. In a remote setting, consider focusing management efforts first on **process**. GitLab operates [handbook-first](/handbook/handbook-usage/#why-handbook-first), which is to say that everything which can be documented, is. 

To better understand how this impacts management style, consider this example. Each time a manager is asked a question by a direct report, there is a loss of productivity and focus in answering. If this answer is delivered verbally and privately, its benefit is highly specific and ephemeral. If, however, the manager considers the answer, documents it in a searchable location, and [answers with a link](/company/culture/all-remote/self-service/#answer-with-a-link), the process of answering becomes far more useful long-term. 

In the latter case, this act of managing a *process* instead of a *person* creates outsized long-term [efficiency](/handbook/values/#efficiency). Every future direct report who has the same question will now be able to side-step the interruption and locate the answer themselves, creating two positive loops in the process. One, new hires recognize that they are [empowered to search for answers](/company/culture/all-remote/self-service/), securing important information to keep projects moving even when their manager is on vacation, out of the office, or engaged in other work. 

This should lead to fewer blockers, less [dysfunction](/handbook/values/#five-dysfunctions), greater [autonomy](/handbook/values/#give-agency), improved [mental health](/company/culture/all-remote/mental-health/), and greater productivity. Two, managers carve out more bandwidth in their day to focus, rather than re-answering questions. 

## Document solutions

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/u0dRWDmYSvg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [LinkedIn Talent on Tap video](https://youtu.be/u0dRWDmYSvg) above, GitLab co-founder and CEO Sid Sijbrandij shares advice on managing within a remote worplace. Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

It is the job of a manager to ensure a direct report has what they need to be successful on an ongoing basis. By [documenting](/company/culture/all-remote/management/#scaling-by-documenting) process, guides, solutions, how-tos, and policies, a manager is practicing [servant leadership](https://www.shrm.org/resourcesandtools/hr-topics/organizational-and-employee-development/pages/the-art-of-servant-leadership.aspx) in a powerful way. 

If your company has yet to implement their own handbook, start now and start small. Don't be overwhelmed with the notion of building a complete handbook from the get-go; simply start with one process, then document the next, and so on. This is the power of [iteration](/handbook/values/#iteration). GitLab (the company) uses GitLab ([the product](/stages-devops-lifecycle/)) to build and maintain our public-facing [handbook](/handbook/), and options from [Almanac](https://almanac.io/) and [Trainual](https://trainual.com/) are available as well. 

In the event that a direct report asks a question that has yet to be documented, agree to document the eventual solution so that the work put forth in answering benefits a wider swath of people. 

By embracing a documentarian mindset as a manager, you show that you are proactively and [transparently](/handbook/values/#transparency) working to equip your direct report(s) with everything they need to succeed. 

This may feel as if it's placing an added burden on a manager. The reality is that short-term pangs derived from time spent on documenting will be greatly overshadowed by long-term efficiencies. If you, as a manager, believe that you "simply don't have time to document," pause and consider the current scenario from a perspective involving more than yourself. 

If you don't have time to do it right, when will you find time to do it over? 

## Lean on a mentor 

![GitLab all-remote mentor](/images/all-remote/gitlab-commit-sf-2020-mentor.jpg){: .shadow.medium.center}

Even for those who have managed colocated teams for decades, the thought of managing teams which are distributed globally can be daunting. Managing remotely is a skill that can be taught and learned, and much of what is gleaned through colocated management experience can guide one's journey through remote management. 

For those new to managing remotely, consider shadowing someone with experience and establishing a mentor-mentee relationship. 

If you feel comfortable with the softer skills, pay close attention to the processes used by remote managers. For example, you can't walk by a report's desk and get a feel for how things are going, so many remote managers utilize an ongoing [Google Doc agenda](/handbook/leadership/1-1/suggested-agenda-format/) (or a dedicated tool, such as [Soapbox](https://soapboxhq.com/) or [Fellow](https://www.fellow.app/)) where notes, blockers, etc. can be chronicled. Checking a living, evolving document as a mechanism for engagement may require building a new habit.  

## Discuss learning preferences

![GitLab all-remote computer](/images/all-remote/GitLab-All-Remote-Learning-Computer.jpg){: .shadow.medium.center}

Learning is personal. Not only does it vary from person to person, but it can vary from project to project. It's important to understand the [breadth of learning styles](https://blog.mindvalley.com/types-of-learning-styles/), and have continual conversations that take this into account. Many managers will ask their reports to take a personality test, or ask what their preference learning style is, during the first week working together. Unfortunately, that exploration typically ends there.

Remote managers should view this as a perpetual item for discussion. As the relationship evolves, skills are built, and experiences are gained, it's possible that one's preferred style will shift. 

Managers of global teams should also anticipate a variety of styles to be represented in their team. This encourages [diversity](/company/culture/inclusion/), and it requires a manager to be cognizant of what style they're interacting with when bouncing between conversations. In a colocated space, reports may flex their style to more closely align with people they are in physical proximity to. 

## Focus on career progression

![GitLab all-remote computer](/images/all-remote/gitlab-commit-sf-2020-map.jpg){: .shadow.medium.center}

A common concern of remote workers is the perceived inability to further their career while outside of an office. This is often seen in [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, where remote employees may wonder if team members who commute into the office will be better positioned for raises and promotion opportunities.  

Great remote managers will proactively ask about one's [career goals](/handbook/people-group/learning-and-development/career-development/), and frequently discuss how a report is moving towards a particular career objective. 

[Research from Headlamp](https://headlamp.team/engaging-remote-employees-managers-guide/) shows that 82% of workers said they would be more engaged in their work if their managers regularly discussed their career aspirations but only 16% of employees reported having those conversations on a regular basis. By having [regular conversations about career advancement](https://headlamp.team/engaging-remote-employees-managers-guide/#chapter5) with your remote team, you can build a more connected and engaged workforce.

GitLab favors more frequent conversations on this topic — even during [routine 1-1 conversations](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) — as opposed to waiting until an annual review cycle. 

## Remove roadblocks to improve productivity 

In a global all-remote organization, [driving results](/handbook/values/#hierarchy) is a core value. As a manager, you have to keep many balls in the air simultaneously and shift your energy and attention to activities that will produce the greatest output (also known as "[managerial leverage](https://medium.com/@iantien/top-takeaways-from-andy-grove-s-high-output-management-2e0ecfb1ea63)"). Removing roadblocks to improve productivity is a key skillset of any remote manager. Great managers will delegate activities while giving team members the full picture, encourage others to work according to [GitLab's values](/handbook/values/), and set their direct reports up for success. 

To improve managerial productivity: 
1. Create [feedback](/handbook/values/#give-feedback-effectively) mechanisms for team members to understand what they are doing well and what could be improved.
1. Develop incentives that benefit the team and individuals. 
1. Craft training mechanisms for leaders and continuously improve tools to help the team achieve results. 
1. Prioritize tasks and have a clear expectation of what everyone is working on with due dates. 
5. Use GitLab Issues and Merge Requests to track work. 

With GitLab's commitment to [transparency](/handbook/values/#results), team members have a great deal of visibility to what is going on throughout the organization. A manager's role is to focus the team on cross-functional activities relevant to their [results](/handbook/values/#results). 


## Managing non-remote team members

In an [all-remote environment](/company/culture/all-remote/stages/), where every single member works outside of a centralized company office, you won't be put in a situation where a remote manager must manage a non-remote team member.

However, it *is* conceivable that a remote leader would manage a colocated third-party team. For example, a remote public relations (PR) manager overseeing a colocated agency team on contract. In [hybrid-remote companies](/company/culture/all-remote/hybrid-remote/), this scenario is more common, as a subset of the company commutes into a physical office while others work remotely.

This arrangement is best addressed when colocated members adopt [remote-first communication and workflow practices](/company/culture/all-remote/how-to-work-remote-first/). Managing these teams may require additional coaching to use tools like Zoom and Slack in place of in-person communication, even if it feels unnatural, in order to treat everyone as equally as possible. 

For example, if you're leading a syncronous meeting with colocated reports, ask that each person [use their own](/company/culture/all-remote/how-to-work-remote-first/#everyone-must-use-their-own-webcam-no-hybrid-calls) webcam and microphone, and that all [documented discussion](/company/culture/all-remote/handbook-first-documentation/) occur in a shared document. 

## What GitLab managers say about what it means to be a leader

GitLab runs a quarterly [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) program to enable our people leaders with the skills to manage remote teams. We asked managers:

> In your own definition, what does it mean to be leader and manager at GitLab? My job as a manager is to...

Here's what they had to say: 

1. "First and for most my job is to be there for my people - to be their advocate for their career and coach them through various processes at GitLab. As a leader, I am here to make these processes as frictionless as possible. Continuously improving and sharpening our organization as well as the ability to retain talent should be a priority for all managers, and it becomes a big part of leadership when you have to stand up and enforce our values when it's difficult to do so or charge through ambiguity."
2. "My job as a manager is to support and direct my team. Support can mean coaching (on specific tasks or projects, or career path), listening, and guiding our processes towards results. Direct can mean working with the team cross-functionally to define goals & results (what does success look like?) and what actions we take to get there, reinforcing our values and how we apply them, as well as implementing and refining processes to remove roadblocks and reduce friction."
3. "My job as a manager is to create a safe space for the team to thrive in. To support each team member in achieving their own goals and to make our team shines as a whole. As long as it is fun and rewarding to wake up and come to work, each person will be able to feel safe at work while also having fun."
4. "Assist in getting things out of the teams' way so they can work effectively. I also want to be looking out for the next "me". I want to make sure team members are happy with what they are doing and where they are doing it.  If there's another challenge or even a new position that someone wants to try for, I want to be encouraging of that effort and help them any way I can."
5. "First, it means being both a citizen of and ambassador for GitLab. The core values matter and my actions should be a reflection of them. Second, we (GitLab) have done an admirable job providing async/remote analogs to all the signals and support we can find in an office-based environment."
6. "My job as a manager is to support the growth, development, and understanding of my team. It is to mentor each team member to reach and exceed their goals."
7. "My job as a manager is to be a network builder by thinking strategically, understanding and communicating the overall company direction, and aligning people's sense of purpose with where GitLab is going. To support my team by shifting my attention to what's more pressing to them, individually."
8. "To be a leader at GitLab is upholding our values and sharing vision. I believe most people at GitLab are inspired by the direction their career is going and the direction the company is going. If I can tie their daily tasks and work to a vision that is helping propel something at GitLab forward they will feel valued in their work."
9. "I need to share the direction that we need to head in our team, through regular course corrections and cadences. This helps the team select what they feel will best contribute and own part of the strategy."
10. "Help team members reveal and believe in themselves.  Because humans are amazing creatures, I don't believe in a fully-formed & complete sales professional nor do I think anyone is delivering at their highest capability, yet.  My job is to help them identify the "governors" in place (most often their own) and remove them with a menu of my belief in them, and me helping to reset their own expectations of what they can achieve."  

## GitLab Knowledge Assessment: Being a great remote manager

Anyone can test their knowledge on Being a great remote manager by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSfx9CcEag4ZxiNtYAkUJJ9fzKYg51qEYkcpHRVZNaNxjOJVRA/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

We believe that remote managers can learn from one another, and direct reports who admire their remote manager can inform others on how to manage well. If you have an anecdote, tip, or experience to share that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
