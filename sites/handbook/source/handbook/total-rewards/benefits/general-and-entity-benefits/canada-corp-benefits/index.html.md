---
layout: handbook-page-toc
title: "Gitlab Canada Corp"
description: "GitLab Canada Corp benefits specific to Canada based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Canadian Team Members

Benefits are administered by Great West Life. The benefits decision discussions are held by Total Rewards and the Comp Group to elect the next year's benefits by the carrier's deadlines. Total Rewards will notify the team of open enrollment as soon as details become available.

GitLab covers **100%** of team member, spouse, dependents, and/or domestic partner premiums for medical, dental, and vision coverage.

Please review the full [policy document](https://drive.google.com/file/d/14iVKGg4jZ2VsnT5uzitWjZFSxQwS9qn-/view?usp=sharing) for more information on the benefits being offered.

## Group Health Coverage

GitLab offers medical, dental, and vision coverage at no cost to the team member. The benefits are outlined in the following [benefit summary](https://drive.google.com/file/d/1q_378iLs64-wSSizSKlLxkAm01nvDXZO/view?usp=sharing) as well as below. This documentation also has details on the products and services available under the plan. 

### Medical Coverages

**Coverage Summary:**
* Prescription Drugs: 80% - Nil deductible; 90% if purchased at Costco
* Pay Direct Drug Card: Included - Generic Mandatory
* Maximum:	Unlimited
* Fertility Drugs: Unlimited as per all prescription medication
* Smoking Cessations: $500 lifetime maximum
* Vaccines: Included - reasonable & customary maximum
* Major Medical: 80%
* Annual Deductible: None
* Hospitalization: 100% - Bill deductible - Semi-Private
* Orthotic Shoes: $ 300 per benefit year maximum
* Othotic Inserts: combined with shoes
* Hearing Aids: $700 every 5 years
* Paramedical Practitioners: 80% - $300 per practitioner per benefit year maximum
* Included Specialists: Chiropractor, Osteopath, Naturopath, Podiatrist, Chirpodist, Speech Therapist, Psychologist, Social Worker, Massage Therapist, Physiotherapist, Acupuncturist, Dietician, No Doctor's referral required on any paramedical services
* Out of Country: 100% - Nill deductible - Unlimited
* Maximum Duration: 60 consecutive days
* Trip Cancellation: Not Included
* Private Duty Nursing: $5,000 for a maximum of 12 months per condition
* Survivor Benefit: 24 months
* Termination Age: Retirement

#### Akira - Virtual Care

GitLab team members enrolled in Canada Life have the option of Akira Virtual Care. This is a on-demand virtual care solution. It gives members and their family direct access to secure medical consults with friendly, knowledgeable clinicians – 24/7. For more information, please see the [flyer](https://drive.google.com/file/d/1DIRQ2EaAelXKTsxG8o9mS9P0YIGBnj4j/view?usp=sharing). 

### Dental Coverages

**Coverage Summary:**
* Annual Deductible: None
* Basic & Preventative: 80%
* Periodontic & Endodontic:	80%
  * Annual Maximum: $2,000 per benefit year
* Major Restorative Services: 50%
  * Annual Maximum: $2,000 per benefit year
* Orthodontic Services: 50% (children age 6 and older)
  * Lifetime Maximum: $1,500
* Recall visit: Two visits per benefit year
* Scaling & Rooting Units: 10 in any 12 months
* White Filings: Included
* Free Guide: Current
* Survivor Benefit: 24 months
* Termation Age: Retirement

### Vision Coverages

**Coverage Summary:**
* Vision Care: 100% - $200 every 24 months
* Eye Exams: 100% - 1 visit every 24 months - reasonable & customary maximum

## Life Insurance / AD&D

**Coverage Summary:**
* Benefit: 2 times annual earnings to a maximum of $250,000
* Reduction: Reduces by 50% at age 65
* Termination Age: Reduces by 50% at age 65

## Long Term Disability

**Coverage Summary:**
* Benefit: 66.67% of monthly earnings to a maximum of $12,500 (Taxable)
* Monthly Maximum: 12,500
* Tax Status: Taxable
* Elimination Period: 90 days
* Benefit Period: To age 65
* Definition: 2 years own occupation
* Offsets: Primary
* Cost of Living Adjustment: Not included
* Non-Evidence Maximum: 5,600
* Termination: Age 65

## PTO and Vacation Information

GitLab Canada Corp's statutory vacation requirements are found on the [GitLab PTO handbook page](/handbook/paid-time-off/#statutory-vacation-requirements). Vacation time runs concurrently with GitLab PTO. Team members must designate all vacation time taken as `Vacation` in PTO by Roots to ensure that vacation entitlement is properly tracked.

## Sick Pay
Ontario - Employees are entitled to up to three full days of job protected unpaid sick leave every calendar year under the Employment Standards Act (ESA). Leave under ESA runs concurrently with GitLab PTO. Employees must designate any time off for illness as `Out Sick` in PTO by Roots to ensure that annual sick leave entitlement is properly tracked.
 
Quebec - The Act Respecting Labour Standards (ALS) provides for two days of paid absence per calendar year (January 1 to December 31). This leave runs concurrently with GitLab PTO. Employees must designate any time off for illness as `Out Sick` in PTO by Roots to ensure that annual sick leave entitlement is properly tracked.

Québec - [La Loi sur les normes du travail (N-1.1)](http://www.legisquebec.gouv.qc.ca/fr/showdoc/cs/N-1.1) prévoit deux jours d'absence rémunérée par année civile (du 1er janvier au 31 décembre). Ce congé s'exécute simultanément avec les règles de congé de GitLab. Les employés doivent désigner tout congé de maladie par la catégorie «Out Sick» dans PTO par Roots, pour s'assurer que le droit aux congés de maladie annuels est correctement suivi.

## GitLab Canada Corp Parental leave

### Statutory General Entitlement

**Maternity Leave:** Team members who have worked at GitLab for at least 7 consecutive months are entitled to 17 weeks of Maternity Leave. This leave may begin as early as 13 weeks prior to the expected date of birth. If you child is not born within the first 17 weeks of maternity leave, then maternity leave is extended until the date of birth.

**Parental Leave:** All team members are eligible for up to 63 weeks of Parental Leave. This leave may be taken at any time during the first 78 weeks after your child is born, or the day your child comes into your care. Parental leave must be taken in one continuous period. 

### Maternity Leave Payment

* Team members can receive [Employment Insurance Maternity Benefit](https://www.canada.ca/en/employment-social-development/programs/ei/ei-list/reports/maternity-parental.html#h2.1) for up to 15 weeks of Maternity Leave.
* Team members can also receive Employment Insurance Parental Benefit (see below).
* If a team member is eligible to receive Employment Insurance and also eligible to take [GitLab's Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement Employment Insurance payments to ensure the team member receives 100% pay for up to 16 weeks of leave.
* If a team member is ineligible to receive Employment Insurance, but eligible to take GitLab's Parental Leave, GitLab will pay 100% for up to 16 weeks of leave.
* To apply for Employment Insurance, follow the instructions on the [Service Canada website](https://www.canada.ca/en/employment-social-development/programs/ei/ei-list/reports/maternity-parental.html#h2.3).

### Parental Leave Payment

* Team members can receive [Employment Insurance Standard Parental Benefit](https://www.canada.ca/en/employment-social-development/programs/ei/ei-list/reports/maternity-parental.html#h2.1-h3.2) of up to 35 weeks or [Employment Insurance Extended Parental Benefit](https://www.canada.ca/en/employment-social-development/programs/ei/ei-list/reports/maternity-parental.html#h2.1-h3.2) of up to 61 weeks.
* If a team member is eligible to receive Employment Insurance and also eligible to take [GitLab's Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement Employment Insurance payments to ensure the team member receives 100% pay for up to 16 weeks of leave.
* If a team member is ineligible to receive Employment Insurance, but eligible to take GitLab's Parental Leave, GitLab will pay 100% for up to 16 weeks of leave. 
* To apply for Employment Insurance, follow the instructions on the [Service Canada website](https://www.canada.ca/en/employment-social-development/programs/ei/ei-list/reports/maternity-parental.html#h2.3).

### Applying for Parental Leave in Canada

* To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). 
* Pregnant team members must provide a certificate from a health care practitioner certifying their pregnancy to Total Rewards (total-rewards@gitlab.com).

### GitLab Parental Leave Payments and Employment Insurance

The Employment Insurance benefit is government financial assistance to parents who are away from work to care for their newborn or newly adopted child or if they are pregnant or have recently given birth.

The combined amount of Employment Insurance payments and supplementary payments from GitLab cannot exceed a team member's normal weekly earnings. The supplementary payment will not be used to reduce other statutory accumulated benefits such as sick leave, vacation leave or severance pay.

Per the statutory requirements in Canada, a team member is ineligible to receive any Employment Insurance payments if they are receiving full parental leave pay from GitLab.

## Canada Pension Plan (CPP)

Employer contributions are made to the federal Canada Pension Plan (CPP).

## Enrollment  

GitLab has chosen a digital administration platform to manage our benefits administration. Consider this platform as your single source of truth when it comes to benefits, where you can enroll in the plan, update your enrollment information, and view your coverage that will be in effect March 1, 2020.

**For new hires:**
You will be receiving an invitation from `no-reply@collage.co` with a personalized link. Click on that link to set up your profile. Please reach out to the Total Rewards team at `total-rewards@gitlab.com` if you have any questions! Here are some additional [instructions](https://drive.google.com/file/d/0B4eFM43gu7VPRWZFOHBxMUZnaUVoOGxWbzVSNEdiWk9qOVpR/view?usp=sharing) on how to set up your account and enroll in benefits.

### Canada Life Enrollment

To register through Canada Life, you can do so by following this link: https://gwl.greatwestlife.com/MyRegistration. This will allow you access to Canada Life’s GroupNet for Plan Members and the ability to download the GroupNet Mobile app on your mobile device. Once registered you will have access to online claims submission, your electronic benefits card, direct deposit of claim payments, view the status of your claims and more. Canada Life has also provided the following [Welcome Packet](https://drive.google.com/file/d/1Fg9LxE-eyLqgDihpw5kUb-4PuDK5AeMa/view?usp=sharing). The [french version of the welcome packet](https://drive.google.com/file/d/1SChNz6Ir6Vyk8scbM-WJR1N8solVmpeE/view?usp=sharing) is also available. 

Learn how to use GroupNet for Plan Members by watching the helpful videos in the following [pdf](https://drive.google.com/file/d/1mrZPMKd17m9cwC1Qnsdq6KyJ2lmfehFe/view?usp=sharing).

## Insurance Cards

Printed Canada Life benefits cards are being phased out and it is recommended to print a PDF card for yourself and your spouse (if applicable). You can also access your electronic drug card online through the Groupnet for Plan Members site or through the mobile app.

For further instructions on how this can be obtained online, please review the following [PDF](https://drive.google.com/file/d/1x3Ug5nYQir-Ct_8YxN2dwBP0b7uMvGX9/view?usp=sharing).

This card provides your group plan number and member identification number. Keep this information with you at all times and use it when requested by a health or dental provider or in case of medical emergency when traveling.

## Canada Life Tools

Canada Life has designed [tools and services](https://www.greatwestlife.com/you-and-your-family/products/group-benefits.html) to make your life easier, allowing you to access your coverage details when you want, the way you want.  

## Canada Life User Portal and App 

Canada Life has a [user portal](https://www.canadalife.com/) and App available to download that allows you to access your benefits information and ID cards. Please see the [brochure](https://drive.google.com/file/d/1ohL0svax7CEyfuNl1Xn0HcURwJ9I5HYm/view?usp=sharing) for more information and instructions on registering. 

## Frequently asked questions

* Can the pharmacist submit my prescription claim electronically without a drug card?
  * Yes, as long as you have the necessary information, a physical card is not needed for your prescription claim to be electronically submitted. Here are a few options for you: Log-on to [GroupNet for Plan Members](https://gwl.greatwestlife.com/MyLogin) and print out a paper copy of your drug card. Log-on to GroupNet for Plan Members (Mobile App or online) and save an electronic card to your phone. Provide your pharmacist you plan number, identification number, date of birth and identify if the patient is the employee, spouse or dependent child. 
* Who should I contact with questions?
  * For health, prescription drug, dental or out-of-country coverage or claims inquiries, please call 1-800-957-9777 or email Canada Life at `grp_hd_inquire@gwl.ca`.
* What are my key responsibilities as a plan member?
  * If you experience a major lifestyles event, you must advise your plan administrator within 31 days of the event and complete any corresponding paperwork. Failure to notify Canada Life of a lifestyle event within 31 days of the event, could result in limitations or declined coverage. Please see your plan administrator for support.
  * Major lifestyles events include: 
     * Marriage - legal or common-law (1st day of cohabitation)
     * Legal Separation
     * Divorce or termination of a common-law relationship
     * Death of spouse or insured child, Birth or adoption of a child
     * Loss of spouse’s benefits
     * Dependents becoming ineligible for dependent coverage either because of their age or student status 

### Enrollment of Domestic Partners

Great West Life uses the first day cohabitating as the date of eligibility to enter into the GitLab health plan. Anyone who has a common law spouse that they wish to add, moving from Single to Family coverage, for example, will need to do so within 30 days of moving in together.

## Administrative Processes 

**Adding a Beneficiary Designation Form to Collage:** 
1. Login to [Collage](https://secure.collage.co/)
2. On the home page, navigate to the "Need Help" box and select submit a request. 
3. Fill out the form and attach the beneficiary designation form. 
The team member cannot upload this directly so it must be uploaded on behalf of the team member by the total rewards team. 
